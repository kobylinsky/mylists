Advanced Java Lab 2 - Lists
=
Analogue of java.util.List collection, including such implementations, as: 

MyArrayList
---
Resizable-array implementation of the MyList interface. 

Implements all optional list operations, and permits all elements, including null. 
In addition to implementing the List interface, this class provides methods 
to manipulate the size of the array that is used internally to store the list.

MyLinkedList
---
Linked list implementation of the List interface. 

Implements all optional list operations, and permits all elements (including null). 
In addition to implementing the List interface, the LinkedList class provides uniformly 
named methods to get, remove and insert an element at the beginning and end of the list.

---
_by Bogdan Kobylinsky_
