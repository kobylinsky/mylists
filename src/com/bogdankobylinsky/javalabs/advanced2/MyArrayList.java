package com.bogdankobylinsky.javalabs.advanced2;

import java.util.Arrays;
import java.util.RandomAccess;

/**
 * My implementation of <link>java.util.ArrayList</link>.
 *
 * @author Bogdan Kobylinsky
 */
public class MyArrayList implements MyList, RandomAccess {

    /**
     * Coefficient that determines when the array of the elements should be
     * enwided.
     */
    private float loadFactor = 0.75f;
    /**
     * Actual size of the list.
     */
    private int size = 0;
    /**
     * Array of objects.
     */
    private Object[] objects;

    /**
     * Default constructor.
     */
    public MyArrayList() {
        this(10);
    }

    /**
     * Constructor that initializes the list with the size of <b>capacity</b>.
     *
     * @param capacity is the initial size of list.
     * @throws IllegalArgumentException is the <b>capacity</b> argument is less
     * than 1 or bigger than <link>Integer.MAX_VALUE</link>.
     */
    public MyArrayList(int capacity) throws IllegalArgumentException {
        if (capacity < 1 || capacity > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Capacity argument must be in "
                    + "the range [1,Integer.MAX_VALUE).");
        }
        objects = new Object[capacity];

    }

    /**
     * Adding <b>element</b> to the end of the List.
     *
     * @param element is a refernece to an Object.
     */
    @Override
    public void add(Object element) {
        enwide(1);
        objects[size++] = element;
    }

    /**
     * Adding <b>element</b> to the list in specified <b>index</b> position.
     *
     * @param index is the position for inserting <b>element</b>.
     * @param element is a reference to an Object.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public void add(int index, Object element) throws IllegalArgumentException {
        checkForIndex(index);
        enwide(1);
        for (int i = size; i > index; i--) {
            objects[i] = objects[i - 1];
        }
        objects[index] = element;
        size++;
    }

    /**
     * Adding objects to the end of the list.
     *
     * @param newObjects is the array of objects to be inserted.
     * @throws NullPointerException if the array of added objects is not
     * initialized.
     */
    @Override
    public void addAll(Object[] newObjects) throws NullPointerException {
        if (newObjects == null) {
            throw new NullPointerException("Array of added elements must be "
                    + "initialized.");
        }
        if (newObjects.length > 0) {
            enwide(newObjects.length);
            for (Object object : newObjects) {
                objects[size++] = object;
            }
        }
    }

    /**
     * Adding array of objects <b>newObjects</b> in specified <b>index</b>
     * position.
     *
     * @param index is the position for inserting array.
     * @param newObjects is the array of objects to be inserted.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     * @throws NullPointerException if the array of added objects is not
     * initialized.
     */
    @Override
    public void addAll(int index, Object[] newObjects)
            throws IllegalArgumentException, NullPointerException {
        checkForIndex(index);
        if (newObjects == null) {
            throw new NullPointerException("Array of added elements must be "
                    + "initialized.");
        }
        int newObjectsLength = newObjects.length;
        if (newObjectsLength > 0) {
            enwide(newObjectsLength);
            for (int i = size + newObjectsLength; i > index; i--) {
                objects[i] = objects[i - 1];
            }
            for (int i = 0; i < newObjectsLength; i++) {
                objects[i + index] = newObjects[i];
            }
            size += newObjectsLength;
        }
    }

    /**
     * Getting the link to the object that corresponds to the specified
     * <b>index</b> position in the list.
     *
     * @param index is the position of object in array.
     * @return link to the object that is in <b>index</b> position in the array.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public Object get(int index) throws IllegalArgumentException {
        checkForIndex(index);
        return objects[index];
    }

    /**
     * Removing the object from the list that is in <b>index</b> position.
     *
     * @param index is the posiiton of object.
     * @return the removed object.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public Object remove(int index) throws IllegalArgumentException {
        checkForIndex(index);
        Object removed = objects[index];
        for (int i = index; i < size - 1; i++) {
            objects[i] = objects[i + 1];
        }
        objects[--size] = null;
        return removed;
    }

    /**
     * Setting the object <b>element</b> in specified <b>index</b> position.
     *
     * @param index is the position of the object to be setted.
     * @param element is the new object that wil be setted in <b>index</b>
     * position.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public void set(int index, Object element) throws IllegalArgumentException {
        checkForIndex(index);
        objects[index] = element;
    }

    /**
     * Getting the first index of Object <b>object</b> that is in the list.
     *
     * @param object is the desired object.
     * @return the index of the desired object in the list.
     */
    @Override
    public int indexOf(Object object) {
        if (object == null) {
            for (int i = 0; i < size; i++) {
                if (objects[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (objects[i].equals(object)) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Getting the actual size of the list.
     *
     * @return the actual size of the list.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Getting the list in the array representation.
     *
     * @return the array of objects.
     */
    @Override
    public Object[] toArray() {
        return Arrays.copyOf(objects, size);
    }

    /**
     * Making the list twice wider if array has not enough free space.
     *
     * @param additional is the number of elements which will be added to the
     * list.
     */
    private void enwide(int additional) {
        if (size + additional >= loadFactor * objects.length) {
            objects = Arrays.copyOf(objects, objects.length * 2);
        }
    }

    /**
     * Checking whether specified <b>index</b> is in the range between <b>0</b>
     * and the <b>size</b> of the list.
     *
     * @param index is the specified index of the element in the list.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    private void checkForIndex(int index) throws IllegalArgumentException {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("The index of specified element "
                    + "must be in the range between 0 and the size of the "
                    + "list.");
        }
    }
}
