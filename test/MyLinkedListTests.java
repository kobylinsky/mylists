
import com.bogdankobylinsky.javalabs.advanced2.MyLinkedList;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Tests of <link>com.bogdankobylinsky.javalabs.advanced2.MyLinkedList</link>
 * class.
 *
 * @author Bogdan Kobylinsky
 */
public class MyLinkedListTests {

    private MyLinkedList myList;

    /**
     * Testing
     * <code>size()</code> and
     * <code>add(Object o)</code> methods.
     */
    @Test
    public void testMyList_Size() {
        myList = new MyLinkedList();
        assertEquals(0, myList.size());
        for (int i = 0; i < 100; i++) {
            myList.add("newObject_#" + i);
        }
        assertEquals(100, myList.size());
    }

    /**
     * Testing
     * <code>add(Object o)</code> and
     * <code>get(int index)</code> methods.
     */
    @Test
    public void testMyList_Add_Get() {
        myList = new MyLinkedList();
        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList.add(0, "0.1");
        assertArrayEquals(new String[]{"0.1", "1", "2", "3"}, myList.toArray());

        myList.add(3, "2.99");
        assertArrayEquals(new String[]{"0.1", "1", "2", "2.99", "3"}, myList.toArray());
    }

    /**
     * Testing
     * <code>addAll(Object[] o)</code>,
     * <code>addAll(int index, Object[] o)</code>, and
     * <code>get(int index)</code>,
     * <code>size()</code> methods.
     */
    @Test
    public void testMyList_AddAll() {
        myList = new MyLinkedList();
        myList.addAll(new String[]{"1", "2", "3"});
        assertEquals("2", myList.get(1));

        myList.addAll(1, new String[]{"1.1", "1.2", "1.3"});
        assertEquals("1.3", myList.get(3));
        assertEquals(6, myList.size());
    }

    /**
     * Testing
     * <code>add(Object o)</code>,
     * <code>remove(int index)</code>, and
     * <code>get(int index)</code>,
     * <code>set(int index, Object o)</code> methods.
     */
    @Test
    public void testMyList_Get_Remove_Set() {
        myList = new MyLinkedList();
        for (int i = 0; i < 99; i++) {
            myList.add("newElement_" + i);
        }
        assertEquals("newElement_86", myList.get(86));
        assertEquals("newElement_0", myList.remove(0));
        assertEquals("newElement_1", myList.get(0));
        myList.set(12, "SETTED_new13");
        assertEquals("SETTED_new13", myList.get(12));
    }

    /**
     * Testing
     * <code>add(Object o)</code>,
     * <code>indexOf(Object o)</code> methods.
     */
    @Test
    public void testMyList_IndexOf() {
        myList = new MyLinkedList();
        for (int i = 0; i < 99; i++) {
            myList.add("newElement_" + i);
        }
        assertEquals(86, myList.indexOf("newElement_86"));
        myList.add("LAST ONE");
        assertEquals(99, myList.indexOf("LAST ONE"));
        myList.add("LAST ONE");
        assertEquals(99, myList.indexOf("LAST ONE"));

        assertEquals(-1, myList.indexOf("NO_SUCH_STRING_IN_THE_LIST"));
    }

    /**
     * Testing
     * <code>add(Object o)</code>,
     * <code>toArray()</code>,
     * <code>set(int index, Object o)</code> methods.
     */
    @Test
    public void testMyList_ToArray() {
        myList = new MyLinkedList();
        Object[] testStrings = new Object[]{"1", "2", "3", "4", "5"};
        for (Object string : testStrings) {
            myList.add(string);
        }
        assertArrayEquals(testStrings, myList.toArray());

        testStrings[0] = "new1";
        myList.set(0, "new1");
        assertArrayEquals(testStrings, myList.toArray());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMyList_Illegal1() {
        myList = new MyLinkedList();
        myList.set(-1, "fail");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMyList_Illegal2() {
        myList = new MyLinkedList();
        myList.add(42, "fail");
    }

    @Test(expected = NullPointerException.class)
    public void testMyList_Illegal3() {
        myList = new MyLinkedList();
        myList.addAll(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMyList_Illegal4() {
        myList = new MyLinkedList();
        myList.addAll(42, new String[]{"fail"});
    }

    @Test(expected = NullPointerException.class)
    public void testMyList_Illegal5() {
        myList = new MyLinkedList();
        myList.addAll(new String[]{"norm"});
        myList.addAll(0, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMyList_Illegal6() {
        myList = new MyLinkedList();
        myList.addAll(new String[]{"norm"});
        myList.get(42);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMyList_Illegal7() {
        myList = new MyLinkedList();
        myList.get(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMyList_Illegal8() {
        myList = new MyLinkedList();
        myList.remove(-1);
    }
}
